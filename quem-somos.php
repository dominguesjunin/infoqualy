<?php include('header.php'); ?>

    <div class="banner full only"></div>

    <section class="conteudo full quem-somos">
        <div class="content-box">
            <!-- sobras dos cantos -->
            <div class="shadow shadow-left"></div>
            <div class="shadow shadow-right"></div>
            
            <article class="center">
                <h1 class="title full">Quem somos</h1>
                <div class="clearfix"></div>

                <div class="left">
                    <p>
                        <img class="right" src="img/img1.jpg" alt="">
                        A INFOQUALY é uma plataforma de gestão online para ações de marketing direto e database marketing, que garante uma comunicação sem dispersão. Além disso, permite ao cliente acompanhar de forma online e em tempo-real o andamento das campanhas em seus mais diversos aspectos.
                    </p>

                    <h2>OFERECEMOS:</h2>
                    <ul>
                        <li>+ Enriquecimento de grandes bases de dados.</li>
                        <li>+ Extração de listas qualificadas para ações de prospecção de clientes.</li>
                        <li>+ Envio de SMS e Email Marketing para uma base de dados própria ou adquirida na ferramenta.</li>
                    </ul>
                    <p>Fazemos parte de um grupo econômico internacional focado em soluções verticalizadas de tecnologia e infraestrutura. Sendo assim, possuímos empresas dentro do nosso grupo com foco de trabalho auto dirigido, voltado ao desenvolvimento de softwares personalizados, importação de equipamentos, integração, telecomunicações e tratamento de dados. Nosso objetivo é integrar nossas soluções às necessidades dos clientes com o objetivo de proporcionar resultados otimizados e redução de custos de mobilização.</p>
                    <p>Além de tecnologia, possuímos um dos maiores e mais completo banco de dados do Brasil.</p>
                    <p>Nossas soluções inteligentes aumentam as negociações e reduzem o risco, pois além de ferramentas adequadas, temos profissionais especialistas em marketing direto, database marketing, crédito e inteligência de negócios.</p>
                    <p>Muitas vezes, o Marketing é acusado de ser o vilão em histórias de fracassos nas empresas. O problema, na maioria dos casos, está na falta de estudos que balizem as ações e os projetos realizados.</p>
                    <p>É neste contexto que se insere a INFOQUALY, uma empresa desenvolvida para ajudar o cliente a determinar o potencial de consumo de uma região, implantação de uma nova marca, localização de pessoas ou empresas a nível Brasil, e principalmente, atuar nas estratégias de expansão dos negócios dos clientes e inteligência de mercado.</p>
                    <p>Nossos projetos estão baseados na PRECISÃO das nossas informações, na QUALIDADE do nosso atendimento/serviços e na VELOCIDADE da nossa entrega.</p>
                    <p>Queremos ressaltar que não faltam justificativas para a realização de um trabalho como o que a INFOQUALY se propõe, visto que:</p>
                    <ul>
                        <li>+ O assunto é crucial para a sobrevivência e a perpetuação de uma empresa.</li>
                        <li>+ A medida que as pequenas, médias ou grandes empresas não desenvolvem ações voltadas para clientes e não se preocupam com a estruturação de um banco de dados ou tecnologia, elas perdem em competitividade.</li>                        
                    </ul>
                    <h2>OS QUATRO PILARES DO NOSSO SUCESSO</h2>
                    <p>
                        <strong>DADO</strong><br/>
                        Relevante, Padronizado e Atualizado
                    </p>
                    <p>
                        <strong>TECNOLOGIA</strong><br/>
                        Correta, Prática, Inteligente, Amigável, Flexível e do Tamanho do Seu Negócio
                    </p>
                    <p>
                        <strong>PESSOA</strong><br/>
                        A Qualidade de Seu Time de Profissionais é o Diferencial
                    </p>
                    <p>
                        <strong>PROCESSOS</strong><br/>
                        Baseado em Conceitos e Metodologias Eficientes
                    </p>
                    <p>
                        <strong>MISSÃO</strong><br/>
                        Criar produtos e serviços que desenvolvam o conhecimento, habilidade e a atitude de nossos clientes, tanto na condução de seus projetos de dados, comunicação e marketing direto, quanto no tratamento de informações, gerando retorno financeiro e maior conhecimento sobre a sua carteira de clientes.
                    </p>
                    <p>
                        <strong>VISÃO</strong><br/>
                        Ser reconhecido pelos nossos clientes como uma empresa fornecedora de soluções para problemas existentes nas operações de geração de leads, comunicação e database marketing.
                    </p>
                    <p>
                        <strong>VALORES</strong><br/>
                        <ul>
                            <li>
                                <strong>Credibilidade:</strong> atender as necessidades e problemas dos nossos clientes em qualquer circunstância.
                            </li>
                            <li>
                                <strong>Confiança e Reputação:</strong> prometer e cumprir de maneira consistente e previsível.
                            </li>
                            <li>
                                <strong>Facilidade: </strong>empregar todos os esforços e ferramentas possíveis com o objetivo de realizar uma tarefa específica com sucesso.
                            </li>
                            <li>
                                <strong>Excelência: </strong>fazer a diferença nos projetos dos nossos clientes, criando aproximação e valores duradouros.
                            </li>
                            <li>
                                <strong>Antecipação: </strong>a partir de competências próprias, devemos criar habilidades que consigam a partir de um diálogo com os nossos clientes mostrar caminhos possíveis para um resultado mais rentável.
                            </li>
                            <li>
                                <strong>Agilidade: </strong>executar os trabalhos de maneira rápida conforme necessidade de cada cliente.
                            </li>
                        </ul>
                    </p>
                </div>
            </article>

            <?php include('footer-interno.php'); ?>  
        </div>
    </section>
    
    <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2CkiBty1wNKfYFX0mYzwqOHv7vttzJT2';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
    
<?php include('footer.php'); ?>    