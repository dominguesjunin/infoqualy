<?php include('header.php'); ?>

    <div class="banner full only"></div>

    <section class="conteudo full quem-somos">
        <div class="content-box">
            <!-- sobras dos cantos -->
            <div class="shadow shadow-left"></div>
            <div class="shadow shadow-right"></div>
            
            <article class="center">
                <h1 class="title full">Consultas online</h1>
                <div class="clearfix"></div>
                <div class="left">
                    <h2>CONSULTAS ONLINE</h2>
                    <i>“Automação de consultas à informações de diversas fontes”</i><br/>
                    <h2>CONSULTAS CADASTRAIS E CRÉDITO</h2>
                    <p>Reunimos informações de diversas fontes na internet em um único ambiente com acesso online e resultado em tempo real.</p>
                    <p><strong>Pré-Análise -> </strong>concessão de crédito, prevenção a fraude.</p>
                    <p><strong>Localizar -> </strong>potenciais clientes, inadimplentes.</p>
                    <p><strong>Consultar -> </strong>dados cadastrais, consultas customizadas.</p>

                    <h2>CONSULTAS VEICULARES</h2>
                    <i>“Perícia Online”</i><br/>
                    <p>A solução PERÍCIA ONLINE da INFOQUALY, está focada na prestação de serviços de consultas veiculares e de proteção ao crédito. Antecedendo a necessidade de informações cadastrais, desenvolveu parcerias que possibilitou oferecer uma gama de informações de qualidade no mercado corporativo e varejo.</p>
                    <p>Nosso maior objetivo é a satisfação de nossos clientes, fornecendo informações que possibilitem a concretização de negócios de forma segura e ágil, quando da concessão de crédito por parte das empresas de um modo em geral, com conseqüente redução dos riscos de inadimplência ou na aquisição, venda e/ou troca de veículos. Com essas informações os negócios são realizados com maior segurança e confiabilidade.</p>

                    <h2>BASE NACIONAL (BIN)</h2>
                    <p>São todas as informações de fabricação do veículo inseridas no DENATRAN como marca, modelo, motor, chassi, câmbio, cor, UF de faturamento, CNPJ da concessionária que faturou OKM e restrições. Ao realizar a consulta BASE NACIONAL e retornar divergência nos dados do documento é importante consultar a BASE ESTADUAL, pois o veículo pode ter sofrido alterações que foram homologadas no DETRAN.</p>

                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <th>Fique de olho</th>
                        </tr>
                        <tr>
                            <td>
                                São todas as informações de fabricação do veículo inseridas no DENATRAN como marca, modelo, motor, chassi, câmbio, cor, UF de faturamento, CNPJ da concessionária que faturou OKM e restrições. Ao realizar a consulta BASE NACIONAL e retornar divergência nos dados do documento é importante consultar a BASE ESTADUAL, pois o veículo pode ter sofrido alterações que foram homologadas no DETRAN.
                            </td>
                        </tr>
                    </table>

                    <h2>BASE ESTADUAL (SÃO PAULO E OUTROS ESTADOS)</h2>
                    <p>Todos os dados constantes no DETRAN sobre a atual situação do veículo em circulação, são retornados nesta consulta. Caso haja alguma alteração nas informações do veículo (mudança de cor, combustível, remarcação de chassi, e outras). Ex: Veículo mudou de cor, então teremos na BASE NACIONAL a cor original e na BASE ESTADUAL a cor atual. É sempre importante comparar com a BASE NACIONAL.</p>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <th>Fique de olho</th>
                        </tr>
                        <tr>
                            <td>
                                Com a BASE ESTADUAL é possível confirmar na base de dados do DETRAN Estadual os dados do veículo, de alterações de originalidade ocorridas após o registro inicial do veículo, restrições e bloqueios, e a existência de débitos de IPVA e Multas. A consulta traz também em detalhes as Restrições Judiciais informadas no RENAJUD (Restrições Judiciais de Veículos Automotores). Evite comprar veículos com problemas já registrados no CNJ (Conselho Nacional de Justiça).
                            </td>
                        </tr>
                    </table>

                    <h2>BASE AGREGADOS</h2>
                    <p>Os dados da BIN (Base de Índice Nacional), são retornados nesta consulta. Ao realizar a consulta, caso identificar que um dos agregados não é o mesmo da numeração original, repita com os dados disponíveis para que o sistema identifique a origem do agregado, ou se pertence a um veículo em circulação ou roubado e tenha sido adulterado para não ser identificado.</p>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <th>Fique de olho</th>
                        </tr>
                        <tr>
                            <td>
                                Consulta e confirma através de chassi, motor ou câmbio do veículo os dados de registro. Este resultado é fornecido com base na BIN (Base de Índice Nacional). É muito utilizada em negociações de motores, câmbio, etc, evitando compra de peças roubadas e/ou furtadas.
                            </td>
                        </tr>
                    </table>

                    <h2>GRAVAME</h2>
                    <p>A consulta SNG (Sistema Nacional de Gravame), retorna se houver, o gravame do veículo, com as informações do financiamento, do financiado (nome e CPF) e dados da financeira (razão social e cnpj), além do status atual do gravame se está baixado, cancelado ou financiado.</p>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <th>Fique de olho</th>
                        </tr>
                        <tr>
                            <td>
                                Evite a compra de veículos ainda com restrições financeiras e conheça o último proprietário.
                            </td>
                        </tr>
                    </table>

                    <h2>LEILÃO</h2>
                    <p>Os veículos que passaram por leilão de seguradoras, bancos, e outros, são retornados nesta consulta. Base de dados contendo milhões de veículos com fotos, recolhidos de PT (Perda Total), recuperados de furtos, enchentes, busca e apreensão.</p>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <th>Fique de olho</th>
                        </tr>
                        <tr>
                            <td>
                                Mais de um leilão para o veículo consultado pode ser retornado, uma vez que suas condições tenham sido alteradas. Para uma análise mais a fundo é necessário realizar a consulta Perda Total (PT), porque nem todo veículo de Sinistro de Indenização Integral é leiloado e nem todos os carros de leilão são de seguradoras. Podem ser recuperados de financiamentos, em pátios dos DETRANs, etc.
                            </td>
                        </tr>
                    </table>

                    <h2>PERDA TOTAL</h2>
                    <p>A consulta PERDA TOTAL disponibiliza acesso a todos os veículos que constam na base com Sinistro de Indenização Irrecuperável lançados por todas as seguradoras conveniadas à FENASEG, e que podem estar em circulação novamente. </p>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <th>Fique de olho</th>
                        </tr>
                        <tr>
                            <td>
                                Informa os veículos que estão cadastrados na base de dados mantida por todas as seguradoras conveniadas à FENASEG: Veículos Sinistrados com Indenização Integral. A maioria das seguradoras não emite apólices para veículos cadastrados nesta base. As que emitem, possuem valores maiores que os de mercado para o mesmo veículo.                                
                            </td>
                        </tr>
                    </table><br/><br/><br/>
                </div>
                                
            </article>

            <?php include('footer-interno.php'); ?>    
        </div>
    </section>
    
    <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2CkiBty1wNKfYFX0mYzwqOHv7vttzJT2';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
    
<?php include('footer.php'); ?>    