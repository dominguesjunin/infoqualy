<?php include('header.php'); ?>

    <div class="banner full only"></div>

    <section class="conteudo full quem-somos">
        <div class="content-box">
            <!-- sobras dos cantos -->
            <div class="shadow shadow-left"></div>
            <div class="shadow shadow-right"></div>
            
            <article class="center">
                <h1 class="title full">Lista qualificada online</h1>
                <div class="clearfix"></div>
                <div class="left">
                    <h2>IDENTIFICANDO EMPRESAS OU CONSUMIDORES PARA NOVAS OPORTUNIDADES DE NEGÓCIOS</h2>
                    <p>Através da carteira de clientes ou características definidas, esta ferramenta identifica empresas, pessoas ou veículos com as mesmas características de pessoas que já são clientes, deixando em evidência o público-alvo potencial.</p>
                    <p>Antes de realizar ações de marketing direto, como o envio de mala direta ou campanhas de telemarketing, utilize esta solução. A definição de perfil é essencial para qualquer projeto de marketing.</p>
                    <p>Uma lista de contatos qualificados, normalizados e com informações cadastrais atualizadas, serão disponibilizadas aos nossos clientes, com um único objetivo: divulgação de produtos e serviços para quem realmente interessa, e conseqüentemente, aumento nas vendas.</p>
                    
                    <h2>O PRÓPRIO CLIENTE ACESSA A FERRAMENTA, ESCOLHE OS CLIENTES-ALVO, E FAZ A EXTRAÇÃO. MUITO FÁCIL!</h2>
                    <i>Passo-a-Passo</i><br/>
                    <p>O cliente define o público-alvo através do seu know-how, ou através do envio de uma base de dados de seus clientes à INFOQUALY, que faz a escolha por você, utilizando modelos matemáticos de propensão e segmentação (cluster-clonagem):</p>
                    <p><strong>Pessoa Física: </strong>renda presumida, sexo, idade, score, região, pessoas politicamente expostas, etc.</p>
                    <p><strong>Pessoa Jurídica: </strong>faturamento presumido, data de fundação, score, porte, restrições, ramo de atividade das empresas, empresas comercialmente ativas, etc.</p>
                    <p><strong>Veículos: </strong>marca, modelo, ano, região, etc.</p>

                    <h2>ANÁLISE DE CARTEIRA</h2>
                    <p>Para conhecer em maior profundidade o universo disponível de empresas ou consumidores, é essencial o Estudo Potencial de Mercado, que levará em conta a carteira dos seus já clientes, identificando prospects potenciais. Ou seja, qual o perfil-alvo da minha carteira?</p>

                    <h2>MARKET SHARE (PARTICIPAÇÃO DE MERCADO)</h2>
                    <p>Qual é a sua participação no mercado atual? Quantas Empresas ou Consumidores ainda faltam conhecer ou comprar os seus produtos e serviços?</p>
                    <p><img src="img/img2.jpg" alt=""></p>
                    <p>
                        <strong>Selo de Garantia</strong><br/>
                        Estamos acima da média de mercado:<br/>
                        95% para endereço<br/>
                        75% para email<br/>
                        85% para telefone
                    </p>
                    <p>Com uma equipe altamente especializada e focada no “cuidar” da informação, temos excelência no que fazemos, no que entregamos e no apoio ao cliente. </p>
                    <br/><br/><br/>
                </div>
                                
            </article>

            <?php include('footer-interno.php'); ?>  
        </div>
    </section>
    
    <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2CkiBty1wNKfYFX0mYzwqOHv7vttzJT2';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
    
<?php include('footer.php'); ?>    