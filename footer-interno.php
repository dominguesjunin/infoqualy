<footer class="full">
    <div class="center">
        <p class="left">
            Email: <a href="mailto:negocios@infoqualy.net.br">negocios@infoqualy.net.br</a><br/>
            Telefone: (11) 3221-3887
        </p>
        <p class="right">
            Rua do Arouche - 157 - Salas 53/54<br/>
            República - São Paulo - SP<br/>
            CEP: 01219-001
        </p>
    </div>
</footer>