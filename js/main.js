jQuery(document).ready(function($) {
	$("ul.servicos li:nth-child(3n)").addClass("no-margin");

	$('.view').carouFredSel({
		auto: 8000,
		scroll : {
			fx : "crossfade"
		}
	});

	$('.send-form').submit(function() {
		
		if ($('#nome').val() == '') 
		{
			alert('Cinfira se você preencheu todos os campos com asterisco.');
			$('#nome').focus();
			return false;
		}
		else if($('#email').val() == '')
		{
			alert('Cinfira se você preencheu todos os campos com asterisco.');
			$('#email').focus();
			return false;
		}
		else if($('#telefone').val() == '')
		{
			alert('Cinfira se você preencheu todos os campos com asterisco.');
			$('#telefone').focus();
			return false;
		}

		return true;

	});

	$(window).scroll(function(){
		if($(window).scrollTop() > 200)
		{
			$("#back-to-top").fadeIn(200);
		}
		else{
			$("#back-to-top").fadeOut(200);
		}


	});
	
	$('#back-to-top, .back-to-top').click(function() {
		  $('html, body').animate({ scrollTop:0 }, '800');
		  return false;
	});
});
