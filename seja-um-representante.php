<?php 
error_reporting(0);
require_once('lib/class.phpmailer.php');

if ($_POST)
{   
    $mail = new PHPMailer();    
    $mail->CharSet = 'utf-8';
    $mail->SetFrom('negocios@infoqualy.net.br', 'Infoqualy Website'); //remetente
    $mail->AddAddress('negocios@infoqualy.net.br', "Infoqualy Website"); //destinatario
    $mail->Subject = utf8_decode('Solicitação: Seja um Representante'); //assunto do email
    
    //Configurações conforme regras do UOLHOST
    $mail->IsSMTP();                      
    $mail->SMTPAuth   = true;
    //$mail->SMTPDebug=true; //para ver detalhamento de erros (teste)
    $mail->Host       = "smtp.dotcase.com.br";  
    $mail->Port       = 587;                    
    $mail->Username   = "appsender@dotcase.com.br";
    $mail->Password   = "dotcase";

    // Ddos do form
    $nome = $_POST['nome'];
    $email = $_POST['email'];
    $estados = $_POST['estados'];
    $cidade = $_POST['cidade'];
    $telefone = $_POST['telefone'];
    $ramal = $_POST['ramal'];
    $celular = $_POST['celular'];
    $descreva_interesse = $_POST['descreva_interesse'];
            
    //monta o corpo da mensagem
    $body = "<table border='1' width='100%''>
            <tr>
                <td>
                    <strong>Nome</strong>
                </td>
                <td>" . $nome . "</td>
            </tr>
            <tr>
                <td>
                    <strong>E-mail</strong>
                </td>
                <td>" . $email . "</td>
            </tr>
            <tr>
                <td>
                    <strong>Estado</strong>
                </td>
                <td>" . $estados . "</td>
            </tr>
            <tr>
                <td>
                    <strong>Cidade</strong>
                </td>
                <td>" . $cidade . "</td>
            </tr>
            <tr>
                <td>
                    <strong>Telefone</strong>
                </td>
                <td>" . $telefone . "</td>
            </tr>
            <tr>
                <td>
                    <strong>Ramal</strong>
                </td>
                <td>" . $ramal . "</td>
            </tr>
            <tr>
                <td>
                    <strong>Celular</strong>
                </td>
                <td>" . $celular . "</td>
            </tr>
            <tr>
                <td>
                    <strong>Descreva aqui seu interesse</strong>
                </td>
                <td>" . $descreva_interesse . "</td>
            </tr>
        </table>";
    $message = $body;
        
    $mail->MsgHTML(utf8_decode($message));
    
    if ($mail->Send())
    {
        header("location:seja-um-representante.php?message=ok");
    }
    else
    {
        header("location:seja-um-representante.php?message=error");
    }   
}
?>

<?php include('header.php'); ?>
<?php include('estados.php'); ?>

    <div class="banner full only"></div>

    <section class="conteudo full">
        <div class="content-box">
            <!-- sobras dos cantos -->
            <div class="shadow shadow-left"></div>
            <div class="shadow shadow-right"></div>
            
            <article class="center">
                <h1 class="title full">Seja um Representante</h1>
                <div class="clearfix"></div>

                <?php if ($_GET['message'] == 'ok'): ?>
                    <div class="message">
                       <h2>MENSAGEM ENVIADA COM SUCESSO!<br/>EM BREVE ENTRAREMOS EM CONTATO COM VOCÊ!</h2>
                       <p>Para voltar ao formulário <a href="seja-um-representante.php">Clique aqui</a>.</p>
                    </div>
                <?php elseif($_GET['message'] == 'error'): ?>
                    <div class="message">
                        <h2>
                            FALHA NO ENVIO DA MENSAGEM
                        </h2>
                        <p>Para voltar ao formulário <a href="seja-um-representante.php">Clique aqui</a>.</p>
                    </div>
                <?php else: ?>

                <div class="content-all representante">
                    <div class="form">
                        <form action="seja-um-representante.php" method="post" class="send-form">
                            <div>
                                <label for="nome"><span>*</span>Nome do Contato</label>
                                <input type="text" name="nome" id="nome"/>
                            </div>
                            <div>
                                <label for="email"><span>*</span>E-mail</label>
                                <input type="email" name="email" id="email"/>
                            </div>
                            <div>
                                <label for="estados">Estado</label>
                                <select name="estados" id="estados">
                                    <option value="0"></option>
                                    <?php foreach ($estados as $key => $value): ?>
                                        <option value="<?php echo $key ?>"><?php echo $value; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div>
                                <label for="cidade">Cidade</label>
                                <input type="text" name="cidade" id="cidade"/>
                            </div>
                            <div>
                                <label for="telefone"><span>*</span>Telefone Fixo:</label>
                                <input type="text" name="telefone" id="telefone"/>
                            </div>
                            <div>
                                <label for="ramal">Ramal:</label>
                                <input type="text" name="ramal" id="ramal"/>
                            </div>
                            <div>
                                <label for="celular">Telefone Celular:</label>
                                <input type="text" name="celular" id="celular"/>
                            </div>
                            <div>
                                <label for="descreva_interesse">Descreva aqui o seu interesse:</label>
                                <textarea name="descreva_interesse" id="descreva_interesse"></textarea>
                            </div>
                            <div class="form-footer">
                                <input type="submit" value="Enviar"/>
                            </div>
                        </form>
                    </div>
                </div>    
                <?php endif; ?>                
            </article>

            <?php include('footer-interno.php'); ?>  
        </div>
    </section>
    
    <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2CkiBty1wNKfYFX0mYzwqOHv7vttzJT2';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
    
<?php include('footer.php'); ?>    