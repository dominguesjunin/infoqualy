<?php include('../header.php'); ?>

    <div class="banner full only"></div>

    <section class="conteudo full quem-somos">
        <div class="content-box">
            <!-- sobras dos cantos -->
            <div class="shadow shadow-left"></div>
            <div class="shadow shadow-right"></div>
            
            <article class="center">
                <h1 class="title full">Quem somos</h1>
                <div class="clearfix"></div>

                                
            </article>

            <footer class="full">
                <div class="center">
                    <p class="left">
                        Email: negocios@infotools.com.br<br/>
                        Telefone:(11) 3855-2481
                    </p>
                    <p class="right">
                        Av.Engenheiro Caetano Alvares<br/>
                        530 - Barra Funda - São Paulo - SP<br/>
                        CEP 02546-000
                    </p>
                </div>
            </footer>
        </div>
    </section>
    
<?php include('../footer.php'); ?>    