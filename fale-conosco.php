<?php 
error_reporting(0);
require_once('lib/class.phpmailer.php');

if ($_POST)
{   
    $mail = new PHPMailer();    
    $mail->CharSet = 'utf-8';
    $mail->SetFrom('negocios@infoqualy.net.br', 'Infoqualy Website'); //remetente
    $mail->AddAddress('negocios@infoqualy.net.br', "Infoqualy Website"); //destinatario
    $mail->Subject = utf8_decode('Solicitação: Fale Conosco'); //assunto do email
    
    //Configurações conforme regras do UOLHOST
    $mail->IsSMTP();                      
    $mail->SMTPAuth   = true;
    //$mail->SMTPDebug=true; //para ver detalhamento de erros (teste)
    $mail->Host       = "smtp.dotcase.com.br";  
    $mail->Port       = 587;                    
    $mail->Username   = "appsender@dotcase.com.br";
    $mail->Password   = "dotcase";

    // Ddos do form
    $produto = $_POST['produto'];
    $cnpj = $_POST['cnpj'];
    $razao_social = $_POST['razao_social'];
    $website = $_POST['website'];
    $volume_de_dados = $_POST['volume_de_dados'];
    $plataforma_de_envio = $_POST['plataforma_de_envio'];
    $contato = $_POST['contato'];
    $cargo = $_POST['cargo'];
    $email = $_POST['email'];
    $telefone = $_POST['telefone'];
    $celular = $_POST['celular'];
    $mensagem = $_POST['mensagem'];
            
    //monta o corpo da mensagem
    $body = "<table border='1' width='100%''>
            <tr>
                <td>
                    <strong>Produto</strong>
                </td>
                <td>" . $produto . "</td>
            </tr>
            <tr>
                <td>
                    <strong>CNPJ</strong>
                </td>
                <td>" . $cnpj . "</td>
            </tr>
            <tr>
                <td>
                    <strong>Razão Social</strong>
                </td>
                <td>" . $razao_social . "</td>
            </tr>
            <tr>
                <td>
                    <strong>Website</strong>
                </td>
                <td>" . $website . "</td>
            </tr>
            <tr>
                <td>
                    <strong>Volume de Dados</strong>
                </td>
                <td>" . $volume_de_dados . "</td>
            </tr>
            <tr>
                <td>
                    <strong>Já Utiliza Alguma Plataforma De Envio?</strong>
                </td>
                <td>" . $plataforma_de_envio . "</td>
            </tr>
            <tr>
                <td>
                    <strong>Contato</strong>
                </td>
                <td>" . $contato . "</td>
            </tr>
            <tr>
                <td>
                    <strong>Cargo</strong>
                </td>
                <td>" . $cargo . "</td>
            </tr>
            <tr>
                <td>
                    <strong>E-mail</strong>
                </td>
                <td>" . $email . "</td>
            </tr>
            <tr>
                <td>
                    <strong>Telefone</strong>
                </td>
                <td>" . $telefone . "</td>
            </tr>
            <tr>
                <td>
                    <strong>Celular</strong>
                </td>
                <td>" . $celular . "</td>
            </tr>
            <tr>
                <td>
                    <strong>Mensagem</strong>
                </td>
                <td>" . $mensagem . "</td>
            </tr>
        </table>";
    $message = $body;
        
    $mail->MsgHTML(utf8_decode($message));
    
    if ($mail->Send())
    {
        header("location:fale-conosco.php?message=ok");
    }
    else
    {
        header("location:fale-conosco.php?message=error");
    }   
}
?>

<?php include('header.php'); ?>

    <div class="banner full only"></div>

    <section class="conteudo full quem-somos">
        <div class="content-box">
            <!-- sobras dos cantos -->
            <div class="shadow shadow-left"></div>
            <div class="shadow shadow-right"></div>
            
            <article class="center">
                <h1 class="title full">Fale conosco</h1>
                <div class="clearfix"></div>
                <?php if ($_GET['message'] == 'ok'): ?>
                    <div class="message">
                       <h2>MENSAGEM ENVIADA COM SUCESSO!<br/>EM BREVE ENTRAREMOS EM CONTATO COM VOCÊ!</h2>
                       <p>Para voltar ao formulário <a href="fale-conosco.php">Clique aqui</a>.</p>
                    </div>
                <?php elseif($_GET['message'] == 'error'): ?>
                    <div class="message">
                        <h2>
                            FALHA NO ENVIO DA MENSAGEM
                        </h2>
                        <p>Para voltar ao formulário <a href="fale-conosco.php">Clique aqui</a>.</p>
                    </div>
                <?php else: ?>
                                      
                        <p>
                            Agradecemos o seu Interesse!<br/>
                            Preencha o formulário abaixo para nos enviar uma Solicitação de Proposta.
                        </p>
                        <div class="content-all fale-conosco">
                            <div class="form">
                                <form action="fale-conosco.php" method="post" class="send-form">
                                    <div>
                                        <label for="produto">Produto:</label>
                                        <select name="produto">
                                            <option value="Nenhum Produto selecionado"></option>
                                            <option value="SMS">SMS</option>
                                            <option value="E-mail Marketing">E-mail Marketing</option>
                                            <option value="Lista Qualificada">Lista Qualificada</option>
                                            <option value="Enriquecimento De Dados">Enriquecimento De Dados</option>
                                            <option value="Consultas Online">Consultas Online</option>
                                            <option value="iTrade">iTrade</option>
                                        </select>   
                                    </div>
                                    <div>
                                        <label for="cnpj">CNPJ:</label>
                                        <input type="text" name="cnpj" id="cnpj"/>
                                    </div>
                                    <div>
                                        <label for="razao_social">Razão Social:</label>
                                        <input type="text" name="razao_social" id="razao_social"/>
                                    </div>
                                    <div>
                                        <label for="website">WebSite</label>
                                        <input type="text" name="website" id="website"/>
                                    </div>
                                    <div>
                                        <label for="volume_de_dados">Volume De Dados:</label>
                                        <select name="volume_de_dados">
                                            <option value=""></option>
                                            <option value="Até 10.000">Até 10.000</option>
                                            <option value="Até 100.000">Até 100.000</option>
                                            <option value="Até 500.000">Até 500.000</option>
                                            <option value="Até 1.000.000">Até 1.000.000</option>
                                            <option value="Até 2.600.000">Até 2.600.000</option>
                                            <option value="Até 6.000.000">Até 6.000.000</option>
                                            <option value="Acima de 6.000.000">Acima de 6.000.000</option>
                                        </select>   
                                    </div>
                                    <div>
                                        <label for="plataforma_de_envio">Já Utiliza Alguma Plataforma De Envio?</label>
                                        <span class="radio">
                                            <label>
                                                <input name="plataforma_de_envio" value="Sim" type="radio" />
                                                Sim
                                            </label>    
                                        </span>
                                        <span class="radio">
                                            <label>
                                                <input name="plataforma_de_envio" value="Não" type="radio" />
                                                Não
                                            </label>    
                                        </span>
                                    </div>
                                    <div>
                                        <label for="contato">Contato:</label>
                                        <input type="text" name="contato" id="contato"/>
                                    </div>
                                    <div>
                                        <label for="cargo">Cargo:</label>
                                        <input type="text" name="cargo" id="cargo"/>
                                    </div>    
                                    <div>
                                        <label for="email"><span>*</span>E-mail:</label>
                                        <input type="email" name="email" id="email"/>
                                    </div>
                                    <div>
                                        <label for="telefone"><span>*</span>Telefone Fixo:</label>
                                        <input type="text" name="telefone" id="telefone"/>
                                    </div>
                                    <div>
                                        <label for="celular">Telefone Celular:</label>
                                        <input type="text" name="celular" id="celular"/>
                                    </div>
                                    <div>
                                        <label for="mensagem">Mensagem:</label>
                                        <textarea name="mensagem" id="mensagem"></textarea>
                                    </div>
                                    <div class="form-footer">
                                        <input type="submit" value="Enviar"/>
                                    </div>
                                </form>
                            </div>               
                        </div>
                <?php endif; ?>
            </article>

            <?php include('footer-interno.php'); ?>  
        </div>
    </section>
    
    <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2CkiBty1wNKfYFX0mYzwqOHv7vttzJT2';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
    
<?php include('footer.php'); ?>    