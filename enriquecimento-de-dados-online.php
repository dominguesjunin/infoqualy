<?php include('header.php'); ?>

    <div class="banner full only"></div>

    <section class="conteudo full quem-somos">
        <div class="content-box">
            <!-- sobras dos cantos -->
            <div class="shadow shadow-left"></div>
            <div class="shadow shadow-right"></div>
            
            <article class="center">
                <h1 class="title full">Enriquecimento de dados online</h1>
                <div class="clearfix"></div>
                <div class="left">
                    <p>Enriqueça a sua base de dados com novos atributos e obtenha sucesso em suas ações de marketing direto e database marketing.</p>
                    <h2>1. O QUE É?</h2>
                    <p>Nossa maior expertise está em analisar, armazenar, compilar e recuperar grandes volumes de dados e informações. Desta forma, atualizar os dados cadastrais de seus clientes consumidores ou empresas possibilita a aproximação e interatividade com o seu público-alvo de forma imediata, correta e sem desperdícios.</p>
                    <p>Como abordar o cliente sem o telefone ou endereço correto?</p>
                    <p>Deseja enriquecer a sua base de dados com novos atributos e aprofundar o conhecimento sobre este público-alvo de forma ágil e com custos reduzidos?</p>
                    <p>Gostaria de aumentar a conversão em vendas e taxas de retorno em suas campanhas?</p>
                    <h2>2. COMO FUNCIONA?</h2>
                    <p>Primeiramente, defina quais variáveis deseja agregar ao seu banco de dados, informando o layout e formato do arquivo que deseja receber as nossas informações.</p>
                    <p>Todo processamento na ferramenta é feito automaticamente e pelo cliente. Caso o Cliente tenha um Layout específico de ENTRADA e SAÍDA, deverá ser enviado para <a href="mailto:suporte@infotools.com.br">suporte@infoqualy.com.br</a> para o desenvolvimento, e em seguida, disponibilizamos na própria ferramenta.</p>
                    <h2>ATRIBUTOS - CONSUMIDORES</h2>
                    <i>Database Marketing</i><br/><br/>
                    <strong>CADASTRAL:</strong>
                    <ul>
                        <li>+ Endereço Completo (Residencial e Comercial)</li>
                        <li>+ Telefones (Fixo e Celulares)</li>
                        <li>+ Grau Escolar</li>
                        <li>+ Ocupação Profissional</li>
                        <li>+ Data de Nascimento</li>
                        <li>+ Nome da Mãe</li>
                        <li>+ Informações Oficiais da Receita Federal</li>
                    </ul><br/>

                    <strong>INTERNET:</strong>
                    <ul>
                        <li>+ Emails</li>
                    </ul><br/>

                    <strong>RATING:</strong>
                    <ul>
                        <li>+ Renda Presumida</li>
                        <li>+ Modelos de Propensão</li>
                        <li>+ Pessoas Relacionadas</li>
                        <li>+ Beneficiários INSS</li>
                        <li>+ Sócios de Empresas (QSA)</li>
                        <li>+ Informações Veiculares Oficiais</li>
                    </ul><br/>

                    <strong>PRIVATE:</strong>
                    <ul>
                        <li>+ Óbito</li>
                        <li>+ PPE (Pessoas Politicamente Expostas)</li>
                        <li>+ Do Not Call (Não Perturbe)</li>
                        <li>+ Restrições Financeiras</li>
                        <li>+ Bolsa Família (Baixa Renda - 12 milhões)</li>
                    </ul><br/>

                    <h2>ATRIBUTOS - EMPRESAS</h2>
                    <i>Database Marketing</i><br/><br/>
                    <strong>CADASTRAL:</strong>
                    <ul>
                        <li>+ Endereço Completo (Residencial e Comercial)</li>
                        <li>+ Telefones (Fixo e Celulares)</li>
                        <li>+ CNPJ</li>
                        <li>+ Razão Social e Nome Fantasia</li>
                        <li>+ Informações Oficiais da Receita Federal</li>
                        <li>+ Informações Oficiais do Sintegra</li>
                    </ul><br/>

                    <strong>INTERNET:</strong>
                    <ul>
                        <li>+ Emails</li>
                    </ul><br/>

                    <strong>RATING:</strong>
                    <ul>
                        <li>+ Porte da Empresa</li>
                        <li>+ Faturamento Presumido</li>
                        <li>+ Número de Funcionários</li>
                        <li>+ Valor Capital Social</li>
                        <li>+ Empresas Ativas Comercialmente</li>
                        <li>+ Informações Veiculares Oficiais</li>
                        <li>+ Restrições Financeiras</li>
                    </ul><br/>

                    <strong>QSA - SÓCIOS E ADMINISTRADORES:</strong>
                    <ul>
                        <li>+ CPF dos Sócios</li>
                        <li>+ Nome dos Sócios</li>
                        <li>+ Percentual de Participação</li>
                        <li>+ Data de Entrada</li>
                    </ul><br/>
                </div>
                                
            </article>

           <?php include('footer-interno.php'); ?>  
        </div>
    </section>
    
    <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2CkiBty1wNKfYFX0mYzwqOHv7vttzJT2';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
    
<?php include('footer.php'); ?>    