<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,800' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.6.2.min.js"></script>

        <?php 
        $url = $_SERVER['REQUEST_URI']; 
        ?>

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browsehappy">Você está usando um navegador <strong>desatualizado</strong>. Por favor, <a href="http://browsehappy.com/">atualize seu navegador</a> para melhorar a sua experiência.</p>
        <![endif]-->

        <section class="wrap">
            <div class="header full">
                <header class="center">
                    <a href="index.php" class="logo transition"></a>
                    <div id="login">
                        <form action="javascript:void(0);" method="post">
                            <input type="text" name="login" placeholder="login"/>
                            <input type="password" name="pass" placeholder="password"/>
                            <input onclick="alert('Área administrativa em desenvolvimento.');" type="submit" value="ok">
                        </form>
                        <p class="phone">
                            Entre em contato<br/>
                            <strong>(11) 3221-3887</strong>
                        </p>
                    </div>
                    <nav class="navigation full">
                        <ul>
                            <li><a <?php if($url == '/site/index.php'){ echo 'class="active"';} ?> href="index.php">Home</a></li>
                            <li><a <?php if($url == '/site/quem-somos.php'){ echo 'class="active"';} ?> href="quem-somos.php">Quem Somos</a></li>
                            <li class="solucoes">
                                <!-- <a href="solucoes.php">Soluções</a> -->
                                <a href="javascript:void(0);">Soluções</a>
                                <ul class="submenu">
                                    <li><a href="enriquecimento-de-dados-online.php">Enriquecimento de Dados Online</a></li>
                                    <li><a href="lista-qualificada-online.php">Lista Qualificada Online</a></li>
                                    <li><a href="consultas-online.php">Consultas Online</a></li>
                                    <li><a href="sms.php">SMS</a></li>
                                    <li><a href="email-marketing.php">Email Marketing</a></li>
                                    <li><a href="itrade.php">iTrade</a></li>
                                </ul>
                            </li>
                            <li class="solucoes">
                                <!-- <a href="solucoes.php">Soluções</a> -->
                                <a href="javascript:void(0);">Fique Por Dentro</a>
                                <ul class="submenu">
                                    <li><a href="marketing.php">Marketing</a></li>
                                    <li><a href="vendas.php">Vendas</a></li>
                                    <li><a href="qualidade-de-dados.php">Qualidade De Dados</a></li>
                                    <li><a href="o-que-estamos-fazendo.php">O Que Estamos Fazendo</a></li>
                                </ul>
                            </li>
                            <li><a <?php if($url == '/site/seja-um-representante.php'){ echo 'class="active"';} ?> href="seja-um-representante.php">Seja um Representante</a></li>
                            <li><a <?php if($url == '/site/fale-conosco.php'){ echo 'class="active"';} ?> href="fale-conosco.php">Fale Conosco</a></li>
                        </ul>
                    </nav>
                    <div class="clearfix"></div>
                </header>
            </div>