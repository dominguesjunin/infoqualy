<?php include('header.php'); ?>

    <div class="banner full only"></div>

    <section class="conteudo full quem-somos">
        <div class="content-box">
            <!-- sobras dos cantos -->
            <div class="shadow shadow-left"></div>
            <div class="shadow shadow-right"></div>
            
            <article class="center">
                <h1 class="title full">E-mail marketing</h1>
                <div class="clearfix"></div>
                <div class="left">
                    <h2>EMAIL MARKETING</h2>
                    <i>Comunicação Ágil, Personalizada e Dirigida</i><br/>
                    <i>Conquiste novos clientes e amplie sua força de vendas através da internet.</i><br/>
                    <p>
                        <strong>Porque investir neste canal?</strong><br/>
                        - Maneira mais rápida e dinâmica de comunicação empresarial.<br/>
                        - 50 milhões de e-consumidores acessam a internet através de smatphones.<br/>
                        - Suas vendas aumentadas em até 30% (média de crescimento do comércio eletrônico segundo a e-bit).<br/>
                        - Segmentação da lista de destinatários.
                    </p>
                    <p>
                        <strong>Você sabia que:</strong><br/>
                        - No ano passado, nos EUA, um em cada oito casais se conheceram pela Internet?<br/>
                        - No ano passado, mais de 106 milhões de usuários estavam registrados no MySpace? Se o MySpace fosse um país, ele seria o 11º maior do mundo, entre o Japão e o México.<br/>
                        - 2,7 bilhões de perguntas são feitas no Google a cada mês.
                    </p>
                    <p>
                        É uma excelente ferramenta para a divulgação e venda de produtos ou serviços. De forma online, rápida e simples, os relatórios são acessados pelo cliente. 
                        Possuímos base de dados de emails de consumidores e empresas, que podem ser segmentados de acordo com a estratégia do cliente.
                    </p>
                    <p>
                        - Renda Presumida<br/>
                        - Sócio de Empresa<br/>
                        - Região<br/>
                        - Sexo<br/>
                        - Veículo que possui: ano, marca, modelo, data última compra<br/>
                        - e demais atributos.
                    </p>
                    
                    <strong>Passo-a-Passo</strong><br/>
                    <ul>
                        <li>1. Defina o seu público-alvo.</li>
                        <li>2. Dispare os emails, utilizando a ferramenta da INFOQUALY.</li>
                        <li>3. Faça a mensuração dos resultados online.</li>
                        <li>4. Analise, tire a sua conclusão e faça os ajustes necessários para os próximos disparos.</li>
                    </ul>

                    <p>O e-mail cria vínculos com o cliente, fazendo com que ele lembre da organização e retorne ao website. O Email Marketing é uma excelente maneira de criar um relacionamento duradouro com o cliente e reforçar a lembrança da marca.</p>    
                    
                    <p>
                        <strong>Baixo custo</strong><br/>
                        A divulgação via web tem menor custo em relação a qualquer mídia convencional. Além disso, o investimento em Email Marketing traz resultados imediatos (visitas ao seu site), curto prazo (venda indireta) e longo prazo (relacionamento e fidelização).
                    </p><br/><br/><br/>    
                </div>
                                
            </article>

            <?php include('footer-interno.php'); ?>  
        </div>
    </section>
    
    <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2CkiBty1wNKfYFX0mYzwqOHv7vttzJT2';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
    
<?php include('footer.php'); ?>    