<?php include('header.php'); ?>

    <div class="banner full only"></div>

    <section class="conteudo full quem-somos">
        <div class="content-box">
            <!-- sobras dos cantos -->
            <div class="shadow shadow-left"></div>
            <div class="shadow shadow-right"></div>
            
            <article class="center">
                <h1 class="title full">SMS</h1>
                <div class="clearfix"></div>
                <div class="left">
                    <h2>SMS</h2>
                    <p>Aproxime-se Do Seu Cliente. SMS Corporativo, Marketing e Interativo</p>
                    <p><img src="img/img3.jpg" alt=""></p>
                    <p>Metade das agências europeias de Marketing Direto já utiliza o SMS como ferramenta de apoio no planejamento das estratégias de CRM.</p>

                    <h2>QUAIS OS BENEFÍCIOS?</h2>
                    <ul>
                        <li>+ Comunicação em Tempo Real -> a mensagem aparece na tela do celular poucos instantes após o envio.</li>
                        <li>+ Economia -> Baixo Custo e Alto Índice de Penetração (um SMS é mais barato que uma ligação para celular, principalmente se for interurbana).</li>
                        <li>+ Agilidade -> simultaneamente, é possível disparar mais de 2 milhões de SMS por dia. Qual canal de comunicação consegue este efeito?</li>
                        <li>+ Comunicação Personalizada e Segmentada -> o celular é uma mídia pessoal (não é necessário interromper uma reunião para ler um SMS).</li>
                        <li>+ Mais Relevante que Emails!</li>
                        <li>+ Mais Simples e Eficiente que o correio!</li>
                        <li>+ Mais Rápido e Barato que telefonemas!</li>
                    </ul>

                    <h2>QUAL O SEU PÚBLICO-ALVO?</h2><br/>    
                    <strong>Bancos, Financeiras e Recuperadoras de Crédito</strong><br/>
                    <ul>
                        <li>+ Lembrar Vencimentos: para evitar esquecimentos e atrasos nos pagamentos, envie o SMS um dia antes.</li>
                        <li>+ Demandas Receptivas Imediatas: reduza custos de ligações ativas, solicitando o contato de sua carteira de cobrança.</li>
                        <li>+ Débitos Em Aberto: contribua para que a situação do seu cliente fique regularizada. Atrasos no pagamento de dívidas já negociadas são um péssimo negócio.</li>
                        <li>+ Linha Digitável: reduza custos com impressão, distribuição e emissão de boletos. Envie um SMS com a linha digitável.</li>
                    </ul><br/>
                    <p><img src="img/img4.jpg" alt=""></p>

                    <strong>VAREJO</strong>
                    <ul>
                        <li>+ Cartão Da Loja: informar limite disponível para compras aos seus clientes é um incentivo eficaz à utilização do cartão em compras futuras.</li>
                        <li>+ Movimentação No Cartão: envie um SMS quando o cliente fizer qualquer tipo de movimentação na sua loja. O seu cliente terá maior controle no que foi gasto.</li>
                        <li>+ Fidelidade: pontuações, prêmios, promoções, faz com que o seu cliente utilize ainda mais o cartão da sua loja.</li>
                        <li>+ Aniversário: parabenize diariamente os seus clientes, oferecendo desconto em seus produtos, almoço, jantar com direito a acompanhante.</li>
                        <li>+ Parcelas Pendentes: caso não ocorra o pagamento de alguma parcela, envie um SMS.</li>
                        <li>+ Tabelas De Preços: informar a equipe comercial e gerencial referente a reajustes nos preços possibilita rapidez na comunicação.</li>
                        <li>+ Metas: mantenha o seu colaborador informado sobre o seu desempenho.</li>
                        <li>+ Convites e Convocações: lembrete de reuniões, eventos, comemorações, mobiliza pessoas e agiliza no processo de comunicação.</li>
                    </ul>
                    <p><img src="img/img5.jpg" alt=""></p>
                    
                    <strong>Água e energia</strong>    
                    <ul>
                        <li>+ Queda Dos Serviços: problemas em redes de energia elétrica ou abastecimento de água podem ser informados através do SMS, diminuindo os chamados receptivos.</li>
                        <li>+ Agendamento de Visitas: antes de ir até o cliente, informe por SMS o local, data e horário agendado.</li>
                        <li>+ Aviso de Corte de Serviços: alertar o cliente que a água ou energia serão suspensas caso não haja o devido pagamento, evita o constrangimento dos clientes.</li>
                        <li>+ Felicitações: datas comemorativas, aniversários, Natal, Páscoa e Ano Novo – são uma forma de se relacionar com sua Equipe.</li>
                    </ul>
                    <p><img src="img/img6.jpg" alt=""></p>

                    <strong>E-commerce</strong>
                    <ul>
                        <li>+ Validação De Cadastro: o cliente ao finalizar o cadastro no seu site, receberá um SMS validando os dados inseridos. Isto influencia diretamente nas vendas.</li>
                        <li>+ Confirmação De Compra: ao finalizar uma compra, envie online ao cliente por SMS, esta operação. Transforme a relação mais próxima e pessoal.</li>
                        <li>+ Pesquisa de Satisfação: avalie atendimento, entrega, produto e o que quiser com o consumidor que realizou a compra.</li>
                        <li>+ Status da Entrega: como está o processo de entrega de seus produtos? Informe o seu cliente.</li>
                    </ul>
                    <p><img src="img/img7.jpg" alt=""></p>    

                    <h2>NOSSOS DIFERENCIAIS!</h2><br/>
                    <strong>Comunicação</strong>
                    <ul>
                        <li>+ Interface Amigável: O Envio É Realizado Em Poucas Etapas.</li>
                        <li>+ Envios Em Lote Ou Individual</li>
                        <li>+ Criação de Grupos Para Monitoramento Das Campanhas</li>
                        <li>+ Banco De Mensagens Para Próximas Campanhas</li>
                        <li>+ Interrupção Dos Envios A Qualquer Momento</li>
                        <li>+ Criação Do Texto Com Possibilidades De Inclusão De Variáveis</li>
                    </ul><br/>

                    <strong>Relatórios</strong>
                    <ul>
                        <li>+ Total De Envios Ano, Mês E Dia, Representados Por Gráficos.</li>
                        <li>+ Relatório Das Campanhas Enviadas E Respondidas (Interatividade)</li>
                        <li>+ Relatório De Portabilidade (Cobertura Nacional Em Todas As Operadoras)</li>
                        <li>+ Exportação Online De Todos Os Relatórios</li>
                    </ul><br/>

                    <strong>Inteligência</strong>
                    <ul>
                        <li>+ BlackList Procon (Do-Not-Call), BlackList Do Cliente</li>
                        <li>+ Possibilidade De Subir Uma Campanha/Arquivo Em Lote E Fatiar. </li>
                        <li>+ Agendamento Imediato e Futuro</li>
                        <li>+ Enriquecimento De Celulares Diferentes</li>
                        <li>+ Enriquecimento De Score De Probabilidade (Celular É Da Pessoa?)</li>
                        <li>+ Segmentação Brasil Pessoa Física E Jurídica: Por CEP, Por Faixa De Renda, Por Porte Da Empresa, Por Ocupação, Por Ramo De Atividade, e demais atributos.</li>
                        <li>+ Volumetria Pessoa Física -> 189 milhões</li>
                        <li>+ Volumetria Empresas -> 27 milhões</li>
                        <li>+ Possibilidade De Customização Do Layout De Entrada Para Cada Cliente.</li>
                        <li>+ Formatação Automática Do Número Do Telefone: O Sistema Padroniza Automaticamente O Formato Dos Números De Telefones Do Campo “Celular”.</li>
                        <li>+ Workflow De Aprovação: Envie Mensagens De Teste Antes De Disparar A Campanha.</li>
                    </ul><br/>

                    <strong>Insfraestrutura</strong>
                    <ul>
                        <li>+ DataCenter Moderno </li>
                        <li>+ Disponibilidade 24/7</li>
                        <li>+ Acesso Seguro</li>
                        <li>+ Equipe Certificada</li>
                    </ul>

                    <h2>COBERTURA NACIONAL!</h2>
                    <p>O serviço abrange a entrega de mensagem de texto para todas as operadoras que atendem o território nacional.</p>
                    <p><img src="img/img8.jpg" alt=""></p>

                    <h2>COMPROMISSO E QUALIDADE!</h2>
                    <ul>
                        <li>+ Infraestrutura dedicada para cada cliente</li>
                        <li>+ Entregabilidade e Milhões de Mensagens Trafegadas Diariamente</li>
                        <li>+ Clientes de Todos os Tamanhos</li>
                    </ul>
                    <p><img src="img/img9.jpg" alt=""></p><br/><br/><br/>
                </div>
                                
            </article>

            <?php include('footer-interno.php'); ?>  
        </div>
    </section>
    
    <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2CkiBty1wNKfYFX0mYzwqOHv7vttzJT2';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
    
<?php include('footer.php'); ?>    