<?php include('header.php'); ?>

    <div class="banner full only"></div>

    <section class="conteudo full quem-somos">
        <div class="content-box">
            <!-- sobras dos cantos -->
            <div class="shadow shadow-left"></div>
            <div class="shadow shadow-right"></div>
            
            <article class="center">
                <h1 class="title full">iTrade</h1>
                <div class="clearfix"></div>
                <div class="left">
                    <h2>O QUE É</h2>
                    <p>Plataforma para gerenciar seus pontos de vendas (Trademarketing) em tempo real usando cloud e geolocalização.</p>
                    
                    <h2>
                        Você está gerenciando o seu ponto de vendas corretamente?<br/>
                        Você tem certeza?
                    </h2>
                    <strong>Principais Desafios</strong><br/>
                    <ul>
                        <li>+ Falta de produtos na prateleira</li>
                        <li>+ Falha no estoque local da loja</li>
                        <li>+ Falta de rastreabilidade na cadeia logística</li>
                        <li>+ Baixa confiabilidade das informações geradas no local</li>
                        <li>+ Desperdício e perdas de horas de recursos humanos</li>
                        <li>+ Crescimento dos custo em recursos humanos</li>
                        <li>+ Tarefas repetidas para a geração de informação – papel, digitação e envio</li>
                        <li>+ Muitas informações erradas – informações manuais,</li>
                        <li>+ Oportunismo Humano, chegar mais tarde, erro proposital, informações falsas</li>
                        <li>+ Crescente concorrência pelo espaço da loja e pelo cliente</li>
                        <li>+ Problemas de comunicação entre promotor e fábrica</li>
                        <li>+ Descontrole da campanha de degustação, promoção e demostração de produtos no local.</li>
                        <li>+ Aumento do descontrole quando a promoção esta em mais de uma loja ao mesmo tempo.</li>
                    </ul><br/>

                    <strong>Principais Problemas</strong><br/>
                    <ul>
                        <li>+ Perda de vendas para os competidores</li>
                        <li>+ Produtos não posicionados ou indisponíveis para os clientes</li>
                        <li>+ Perda de espaço da loja</li>
                        <li>+ Perdas de produtos em trânsito aumentando os custos</li>
                        <li>+ Informação incorreta direcionando para uma descisão incorreta.</li>
                        <li>+ Aumento de custos com RH por problemas de produtividade e tarefas repetidas</li>
                        <li>+ Custo maior – Menor competitividade</li>
                        <li>+ Riscos por maior dependência humana</li>
                        <li>+ Meta da campanha não atingida sem existir explicação</li>
                        <li>+ Expansão para outros pontos de venda, torna-se inviável</li>
                    </ul><br/>

                    <strong>iTradeware Conceitos Gerais</strong><br/>
                    <ul>
                        <li>
                            + Solução end to end
                            <ul>
                                <li>+ P&D contínuo em marketing e Alta Tecnologia</li>
                                <li>+ Consultoria e Treinamento – Realizado por Mestres e Doutores.</li>
                                <li>+ iTradeware plataforma Cloud</li>
                                <li>+ Gerenciamento desde a implantação até as operações diárias</li>
                            </ul>
                        </li>
                        <li>+ Informações em tempo real</li>
                        <li>+ iTradeware – 99,999% de disponibilidade</li>
                        <li>+ Baseado em Geolocalização</li>
                        <li>+ Interface amigável para promotores – Simples de usar</li>
                    </ul>

                    <h2>SOLUÇÃO ITRADEWARE</h2>
                    <i>Resumo dos Desafios</i><br/>
                    <p><strong>Confiabilidade das Informações -> </strong>Hosting com 99,9999% de disponibilidade</p>
                    <p><strong>Controle de Equipe -> </strong>Rastreamento através de GPS, Wi-fi e triagulação de antenas</p>
                    <p><strong>Flexibilidade na Coleta de Informações -> </strong>Configuração de relatórios na plataforma web e implantação imediata</p>
                    <p><strong>Agilidade na Coleta de Informações -> </strong>Interface de coleta simples através do smartphone</p>
                    <p><strong>Agilidade na extração de relatórios -> </strong>Relatórios em Tempo Real</p>

                    <h2>ITRADEWARE VANTAGENS ESPECIAIS</h2>
                    <ul>
                        <li>+ Interface em Inglês, Portugês e japonês</li>
                        <li>+ Outras línguas sob demanda</li>
                        <li>+ Plataforma Mundial</li>
                        <li>+ Gerenciamento de multiplos contratos</li>
                        <li>+ Multiplos Pontos de Venda(POS)</li>
                        <li>+ Contrato Global e Trabalho Local</li>
                        <li>+ Gerenciamento de multiplas agências</li>
                        <li>+ Gerenciamento de multiplos SKU</li>
                        <li>+ Gerenciamento de multiplas categorias</li>
                    </ul>   

                    <h2>ITRADEWARE CASOS DE USO</h2>
                    <i>iTradeware 1.0</i>
                    <ul>
                        <li>+ Controle de Falta de Produto na Prateleira</li>
                        <li>+ Controle de Reposição de Produto</li>
                        <li>+ Controle de Estoque Local</li>
                        <li>+ Controle de Equipe</li>
                        <li>+ Controle de Campanha</li>
                        <li>+ Controle de preço da concorrência</li>
                        <li>+ Controle de Produtividade dos Atendentes dos Clientes</li>
                    </ul><br/>

                    <strong>Indústrias</strong><br/>
                    <ul>
                        <li>+ Bebidas</li>
                        <li>+ Alimentação</li>
                        <li>+ Cigarros</li>
                        <li>+ Eletrônicos</li>
                        <li>Serviços Bancários</li>
                        <li>Bens de Consumo</li>
                    </ul><br/><br/><br/>
                </div>
                                
            </article>

            <?php include('footer-interno.php'); ?>  
        </div>
    </section>
    
    <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2CkiBty1wNKfYFX0mYzwqOHv7vttzJT2';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->
    
<?php include('footer.php'); ?>    