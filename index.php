
<?php include('header.php'); ?>

    <div class="banner full">
        <div class="banner-content center">
            <div class="slider">
                <div class="view">
                    
                    <div class="item">
                        <img src="img/infoqualy01.jpg" alt="">
                    </div>
                    <div class="item">
                        <img src="img/infoqualy02.jpg" alt="">
                    </div>
                    <div class="item">
                        <img src="img/infoqualy03.jpg" alt="">
                    </div>
                    <div class="item">
                        <img src="img/infoqualy04.jpg" alt="">
                    </div>
                    <div class="item">
                        <img src="img/infoqualy05.jpg" alt="">
                    </div>
                    <div class="item">
                        <img src="img/infoqualy06.jpg" alt="">
                    </div>
                    <div class="item">
                        <img src="img/infoqualy07.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="conteudo full">
        <div class="content-box">
            <!-- sobras dos cantos -->
            <div class="shadow shadow-left"></div>
            <div class="shadow shadow-right"></div>
            
            <article class="center">
                <h1 class="title full">Home</h1>
                <div class="clearfix"></div>
                <p>A INFOQUALY é uma plataforma de gestão online para ações de marketing direto e database marketing, que garante uma comunicação sem dispersão. Além disso, permite ao cliente acompanhar de forma online e em tempo-real o andamento das campanhas em seus mais diversos aspectos.</p>

                <div class="box-servicos">
                    <ul class="servicos">
                        <li>
                            <a href="enriquecimento-de-dados-online.php">
                                <img class="base" src="img/base-de-dados.png" alt=""><br/>
                                <h2>Enriquecimento de Bases de Dados</h2>
                            </a>
                        </li>
                        <li>
                            <a href="lista-qualificada-online.php">
                                <img class="listas" src="img/listas.png" alt=""><br/>
                                <h2>Listas Qualificadas Online</h2>
                            </a>
                        </li>
                        <li>
                            <a href="consultas-online.php">
                                <img class="consultas" src="img/consultas.png" alt=""><br/>
                                <h2>Consultas Online</h2>
                            </a>
                        </li>
                        <li>
                            <a href="sms.php">
                                <img class="sms" src="img/sms.png" alt=""><br/>
                                <h2>SMS</h2>
                            </a>
                        </li>
                        <li>
                            <a href="email-marketing.php">
                                <img class="email" src="img/email.png" alt=""><br/>
                                <h2>Email Marketing</h2>
                            </a>
                        </li>
                        <li>
                            <a href="itrade.php">
                                <img class="itrade" src="img/itrade.png" alt=""><br/>
                                <h2>iTrade</h2>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </article>

            <?php include('footer-interno.php'); ?>  
        </div>
    </section>
    
    <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
$.src='//v2.zopim.com/?2CkiBty1wNKfYFX0mYzwqOHv7vttzJT2';z.t=+new Date;$.
type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->

<?php include('footer.php'); ?>    

